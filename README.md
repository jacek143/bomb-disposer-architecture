# Behavior-based​ ​robot​ ​architecture​ ​in LabVIEW

Developer: Jacek Jankowski
Supervisor: Donald O'Donovan

## Project goals

1. The main goal for this project is to implement behavior-based architecture for a bomb disposal robot that belongs to the Cork Institute of Technology.
2. Created​ ​ software​ ​ should​ ​ allow​ ​ other​ ​ students​ ​ to​ ​ easily​ ​ add​ ​ their​ ​ custom​ ​ code​ ​ and
experiment​ ​ with​ ​ behavioral​ ​ programming​ ​ approach.
3. Software should be delivered together with a few example behaviors showing how to use it.

## Source code

This [repository](https://bitbucket.org/jacek143/bomb-disposer-architecture/overview) contains source code that was developed by Jacek Jankowski. To see how to use it, consult with final_report.pdf in a documents folder.

## Results

The project was carried out by Jacek Jankowski at CIT during the winter semester 2016/2017. On the end the project was submitted to the competition organized by National Instrument and it received financial support from the corporation. During the summer semester the project was carried out by Adam Burke, Craig Davitt and Monika Kozakiewicz. The project finnally was qualified to the TOP 10 of the competion ([see results](https://forums.ni.com/t5/Impulse-Response/Top-10-Student-Projects-Worldwide-2017/ba-p/3619651?espuid=CNATL000001433274&cid=Direct_Marketing-70131000001RowZAAS-Corporate-em113534)).

## Links
1. [facebook fanpage](https://www.facebook.com/GRANTBot/)
3. [competition results](https://forums.ni.com/t5/Impulse-Response/Top-10-Student-Projects-Worldwide-2017/ba-p/3619651?espuid=CNATL000001433274&cid=Direct_Marketing-70131000001RowZAAS-Corporate-em113534)
2. [demonstration video](https://www.youtube.com/watch?v=aV6dPOVMGe8&list=PLRMF3xu4xb56F1-Vqrjuh-NZ2kzh8XTp9)
3. [repository](https://bitbucket.org/jacek143/bomb-disposer-architecture/overview)

